# Vagrant Files
This is a collection of vagrant files for different common stacks.

## To test a vagrant file
- Checkout branch

	`git checkout nodejs`

- Start Vagrant

	`vagrant up`

## Create new vagrant file flavour
- Create a new branch from master. The branch name should be descriptive of the new flavour you are adding.
- Add any puppet modules as git sub modules to puppet/modules/{module-name}.
- Update puppet/manifests/default.pp to provision any packages/services needed for the new flavour.


## Export Vagrantfile to new project without old git information
`git clone --branch nodejs --depth=1 --recursive https://dharrisio@bitbucket.org/501st/vagrant-files.git new-node-project; rm -rf !$/.git`

## To checkout as a submodule
- Add submodule
`git submodule add -b <branch_name> https://dharrisio@bitbucket.org/501st/vagrant-files.git vagrant`

- Update submodule and any submodules contained in it
`git submodule update -b <branch_name> --init --recursive`
